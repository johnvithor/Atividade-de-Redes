package clientSide;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe responsável por possibilitar o login e a interação do Cliente com o
 * Menu de Comunicação
 * 
 * @author Gleydvan e João Vitor
 * @since 15/09/2018
 * 
 */
public class MainClient {
    public static final Scanner leitor = new Scanner(System.in);

    /**
     * Faz o login do usuário e o fornece um menu a esses com opções de comunicação
     * 
     * @param args
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * 
     */
    public static void main(String[] args) {
	int chosenOption = 0;
	boolean inLoginMenu = true;
	boolean inMenu = true;
	Menu menu = null;
	try {
	    menu = new Menu("10.7.102.9", Client.getIP());
	} catch (IOException ioe) {
	    Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ioe);
	    System.err.println(ioe.getMessage());
	    return;
	}
	int countLogin = 0;

	while (inLoginMenu) {
	    if (menu.userLogin())
		inLoginMenu = false;
	    else
		System.out.println("(" + countLogin + ") Tentativa de login Invalida! Tente Novamente ");

	    ++countLogin;
	    if (countLogin > 4) {
		System.out.println("Ultrapassou as tentativas de login! Sistema Encerrado! ");
		inLoginMenu = false;
		inMenu = false;
	    }
	}

	while (inMenu) {
	    System.out.println("----------MENU----------");
	    System.out.println("1 - Criar Anotacao");
	    System.out.println("2 - Buscar Anotacao por Data");
	    System.out.println("3 - Buscar Anotacao por titulo");
	    System.out.println("4 - Substitiur Anotacao");
	    System.out.println("5 - Deletar Anotacao");
	    System.out.println("0 - SAIR");
	    System.out.println("Digite uma das opcoes acima.");

	    String line = leitor.nextLine();
	    try {
		chosenOption = Integer.parseInt(line);
	    } catch (NumberFormatException e) {
		chosenOption = 10;
	    }

	    switch (chosenOption) {
	    case 0:
		System.out.println("Obrigado por utilizar nossos servicos!");

		menu.close();
		inMenu = false;
		break;
	    case 1:
		if (menu.createNote())
		    System.out.println("Operacao realizada com sucesso!");
		else
		    System.out.println("Operacao nao realizada! Por favor tente novamente!");
		break;
	    case 2:
		if (menu.searchNoteByDate())
		    System.out.println("Operacao realizada com sucesso!");
		else
		    System.out.println("Operacao nao realizada! Por favor tente novamente!");
		break;
	    case 3:
		if (menu.searchNoteByTitle())
		    System.out.println("Operacao realizada com sucesso!");
		else
		    System.out.println("Operacao nao realizada! Por favor tente novamente!");
		break;
	    case 4:
		if (menu.replaceNote())
		    System.out.println("Operacao realizada com sucesso!");
		else
		    System.out.println("Operacao nao realizada! Por favor tente novamente!");
		break;
	    case 5:
		if (menu.deleteNote())
		    System.out.println("Operacao realizada com sucesso!");
		else
		    System.out.println("Operacao nao realizada! Por favor tente novamente!");
		break;
	    default:
		System.out.println("Por favor digite uma entrada valida!");
		break;
	    }
	}
    }
}
