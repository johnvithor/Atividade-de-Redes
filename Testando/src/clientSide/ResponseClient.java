package clientSide;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Classe responsável pelo armazenamento das respostas do Cliente
 * 
 * @author João Vitor
 * @since 15/09/2018
 * 
 */

public class ResponseClient {

    private String protocol = "null";
    private String code = "null";
    private String message = "null";
    private String content;
    private boolean keepAlive = true;
    private int timeOut = 3000;
    private Map<String, List> headers = null;

    /**
     * Interpreta a mensagem recebida e a armazena num objeto AnswerClient
     * 
     * @param inputStream
     *            InputStream por onde será lida a resposta que o cliente está
     *            recebendo.
     * @return Um objeto AnswerClient com a resposta devidamente interpretada.
     * @throws IOException
     *             Caso ocorra um erro durante a leitura do InputStream.
     */
    public static ResponseClient read(InputStream inputStream) throws IOException {
	ResponseClient answer = new ResponseClient();
	BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));

	String requestLine = buffer.readLine();

	// System.out.println("Resposta do Server:\n" + requestLine);
	if (requestLine != null) {
	    String[] dadosReq = requestLine.split(" ");
	    if (dadosReq.length < 3) {
		return answer;
	    }

	    // pega o metodo
	    answer.setProtocol(dadosReq[0]);
	    // paga o codigo da resposta
	    answer.setCode(dadosReq[1]);
	    // pega a mensagem de resposta

	    StringBuilder msg = new StringBuilder();
	    for (int i = 2; i < dadosReq.length; i++) {
		msg.append(dadosReq[i] + " ");
	    }
	    answer.setMessage(msg.toString());

	    String headers = buffer.readLine();
	    while (headers != null && !headers.isEmpty()) {
		// System.out.println(headers);
		String[] headerLine = headers.split(":");
		answer.newHeader(headerLine[0], headerLine[1].trim().split(","));
		headers = buffer.readLine();
	    }

	    if (answer.getHeaders().containsKey("Connection")) {
		answer.setKeepAlive(answer.getHeaders().get("Connection").get(0).equals("keep-alive"));
	    }

	    StringBuilder data = new StringBuilder();
	    if (answer.getCode().equals("302") || answer.getCode().equals("409")) {
		String dataBody = buffer.readLine();
		while (!dataBody.equals("end") && buffer.ready()) {
		    data.append(dataBody + '\n');
		    dataBody = buffer.readLine();
		}
		answer.setContent(data.toString());
	    }

	    // System.out.println();
	    // System.out.println(data.toString());
	    // System.out.println("Fim da resposta");
	    return answer;
	}
	return null;
    }

    /**
     * Salva os cabeçalhos e os associam a uma lista de valores no Mapa
     * 
     * @param chave
     *            Nome do cabeçalho
     * @param valores
     *            Lista com um ou mais valores para esta chave
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void newHeader(String chave, String... valores) {
	if (headers == null) {
	    headers = new TreeMap<>();
	}
	headers.put(chave, Arrays.asList(valores));
    }

    /**
     * Retorna os cabeçalhos salvos
     * 
     * @return Retorna um conjunto de cabeçalhos atrelados as suas informações
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public Map<String, List> getHeaders() {
	return headers;
    }

    /**
     * Retorna o protocolo salvo
     * 
     * @return String salva no atributo procolo
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public String getProtocol() {
	return protocol;
    }

    /**
     * Atribui um valor a protocolo
     * 
     * @param procolo
     *            String com um protocolo
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void setProtocol(String protocolo) {
	this.protocol = protocolo;
    }

    /**
     * Retorna o codigo salvo
     * 
     * @return String presente em codigo
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public String getCode() {
	return code;
    }

    /**
     * Atribui um valor a codigo
     * 
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void setCode(String codigo) {
	this.code = codigo;
    }

    /**
     * Retorna uma mensagem salva
     * 
     * @return String presente no atribute msg
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public String getMessage() {
	return message;
    }

    /**
     * Atribui uma mensagem ao atributo msg
     * 
     * @param msg
     *            String com uma mensagem
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void setMessage(String msg) {
	this.message = msg;
    }

    /**
     * Retorna se a conexão deve ser mantida viva
     * 
     * @return boolean presente no atributo manterViva
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public boolean isKeepAlive() {
	return keepAlive;
    }

    /**
     * Atribui o estado de uma conexão a manterViva
     * 
     * @param manterViva
     *            boolean representando o estado da conexão
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void setKeepAlive(boolean manterViva) {
	this.keepAlive = manterViva;
    }

    /**
     * Retorna o tempo limite de conexão
     * 
     * @return int presente no atributo tempoLimite
     * @author João Vitor
     * @since 15/09/2018
     */
    public int getTimeOut() {
	return timeOut;
    }

    /**
     * Atribui um tempo limite
     * 
     * @param tempoLimite
     *            int representando o tempo limite
     * @author João Vitor
     * @since 15/09/2018
     */
    public void setTimeOut(int tempoLimite) {
	this.timeOut = tempoLimite;
    }

    /**
     * Retorna o conteúdo da respota
     * 
     * @return String com o conteúdo da respota
     * @author João Vitor
     * @since 15/09/2018
     */
    public String getContent() {
	return content;
    }

    /**
     * Retorna o tamanho do conteúdo da resposta
     * 
     * @return String com o tamanho em bytes do conteúdo da resposta
     * @author João Vitor
     * @since 15/09/2018
     */
    public String getContentLength() {
	return getContent().length() + "";
    }

    /**
     * Altera o conteudo da resposta.
     * 
     * @param readAllBytes
     *            String com o conteudo da resposta
     * @author João Vitor
     * @since 15/09/2018
     */
    public void setContent(String readAllBytes) {
	content = readAllBytes;

    }
}