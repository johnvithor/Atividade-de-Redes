package clientSide;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import commonSide.Note;

/**
 * Classe que contém as ações realizadas pelas opções do Menu
 * 
 * @author Gleydvan e João Vitor
 * @since 15/09/2018
 * @version
 * 
 */
public class Menu {

    private static final String PATHNOTES = "caminho_para_notas";

    private Client client;
    private Socket socket;
    private String user;
    private List<Note> notes;

    /**
     * Construtor que armazena as entidades da comunicação e incializa uma lista de
     * Notas
     * 
     * @param server
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * @trows IOException
     */
    public Menu(String server, String clientIP) throws IOException {
	client = new Client(clientIP);
	socket = new Socket(server, 8000);
	notes = new ArrayList<>();
    }

    /**
     * Retorna um data com horário
     * 
     * @return um data com horário
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    private String getDataTime() {
	SimpleDateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssX");
	gmtDateFormat.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
	return gmtDateFormat.format(new Date());
    }

    /**
     * Interface de login do Usuário
     * 
     * @return estado do login
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * @throws IOException
     * 
     */
    public boolean userLogin() {
	String login;
	String password;

	System.out.print("Digite o login: ");
	login = MainClient.leitor.nextLine();
	System.out.print("Digite a senha: ");
	password = MainClient.leitor.nextLine();
	ResponseClient answer = null;
	try {
	    long startTime = System.currentTimeMillis();
	    
	    Client.send(socket, client.post("caminho_para_login", login + "-" + password));
	    while (answer == null) {
		answer = ResponseClient.read(socket.getInputStream());
		
		long endTime = System.currentTimeMillis();
		System.out.println("Levou " + (endTime - startTime) + " milesegundos");
		
	    }
	    if (answer.getCode().equals("200")) {
		this.setUser(login);
		return true;
	    }
	    System.out.println("ERRO: " + answer.getMessage());
	    // TODO(Someone): Melhorar este erro.
	} catch (IOException ioe) {
	    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ioe);
	}

	return false;

    }

    /**
     * Busca por notas armazenadas pelo Usuário
     * 
     * @return estado da presença da nota
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * 
     */
    public boolean searchNoteByTitle() {
	String title;

	System.out.print("Digite o titulo da Nota: ");
	title = MainClient.leitor.nextLine();
	ResponseClient answer = null;
	try {
	    Client.send(socket, client.get(PATHNOTES + "-" + user + "-" + title));
	    // answer = ResponseClient.read(socket.getInputStream());

	    while (answer == null) {
		answer = ResponseClient.read(socket.getInputStream());
	    }

	    if (answer.getCode().equals("302")) {
		notes = retrieveNotes(answer.getContent());
		for (Note note : notes) {
		    System.out.println(note.toString());
		}
		return true;
	    } else if (answer.getCode().equals("409")) {
		System.out.println("Mais de uma nota com esse nome:");
		notes = retrieveNotes(answer.getContent());
		for (Note note : notes) {
		    System.out.println(note.toString());
		}
	    } else {
		System.out.println("deu ruim");
		return false;
	    }
	} catch (IOException ioe) {
	    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ioe);
	}

	return true;
    }

    /**
     * Busca por notas armazenadas pelo Usuário por data
     * 
     * @return estado da presença da nota
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * 
     */
    public boolean searchNoteByDate() {
	String date;

	System.out.print("Digite a data da Nota buscada (aaaa/mm/dd): ");
	// TODO(Someone): tratar a entrada para garantir que é uma data.
	date = MainClient.leitor.nextLine();
	ResponseClient answer = null;
	try {
	    Client.send(socket, client.get(PATHNOTES + "-" + user + "-" + date));
	    while (answer == null) {
		answer = ResponseClient.read(socket.getInputStream());
	    }
	    if (answer.getCode().equals("302")) {
		notes = retrieveNotes(answer.getContent());
		for (Note note : notes) {
		    System.out.println(note.toString());
		}
		return true;
	    } else if (answer.getCode().equals("409")) {
		System.out.println("Mais de uma nota com esse nome:");
		notes = retrieveNotes(answer.getContent());
		for (Note note : notes) {
		    System.out.println(note.toString());
		}
	    } else {
		System.out.println("deu ruim");
		return false;
	    }

	} catch (IOException ioe) {
	    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ioe);
	}

	return true;
    }

    /**
     * Solicita informações para que a nota seja criada
     * 
     * @return estado da tentativa da criação da nota
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * 
     */
    public boolean createNote() {
	String title;
	String content;

	System.out.print("Digite o titulo da Nota: ");
	title = MainClient.leitor.nextLine();
	System.out.println("Digite o conteudo da Nota: ");
	content = MainClient.leitor.nextLine();

	Note note = new Note(this.user, content, title, getDataTime());

	ResponseClient answer = null;
	try {
	    long startTime = System.currentTimeMillis();
	    Client.send(socket, client.post(PATHNOTES + "-" + user, note.toString()));
	    while (answer == null) {
		answer = ResponseClient.read(socket.getInputStream());
		long endTime = System.currentTimeMillis();
		System.out.println("Levou " + (endTime - startTime) + " milesegundos");
	    }
	    if (answer.getCode().equals("201")) {
		return true;
	    } else {
		// TODO(Someone): Tratar os possiveis erros
	    }
	} catch (IOException ioe) {
	    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ioe);
	}

	return false;
    }

    /**
     * Solicita informações para que a nota seja deletada
     * 
     * @return estado da tentativa de exclusão da nota
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * 
     */
    public boolean deleteNote() {
	String title;

	System.out.print("Digite o titulo da Nota: ");
	title = MainClient.leitor.nextLine();

	ResponseClient answer = null;
	try {
	    Client.send(socket, client.delete(PATHNOTES + "-" + user + "-" + title));
	    while (answer == null) {
		answer = ResponseClient.read(socket.getInputStream());
	    }
	    if (answer.getCode().equals("200")) {
		return true;
	    } else if (answer.getCode().equals("409")) {
		notes = retrieveNotes(answer.getContent());
		System.out
			.println("Conflito! Mais de uma nota com este titulo, digite o numero da nota a ser removida");
		for (Note note : notes) {
		    System.out.println(note.toString());
		}
		int number = MainClient.leitor.nextInt();
		while (number >= notes.size()) {
		    System.out.println("Digite um numero válido:");
		    number = MainClient.leitor.nextInt();
		}
		Client.send(socket, client.delete(PATHNOTES + "da_lista_enviada-" + user + "-" + title + "-" + number));
		answer = ResponseClient.read(socket.getInputStream());
		if (answer.getCode().equals("200")) {
		    return true;
		}
	    } else {
		// TODO(Someone): Tratar os possiveis erros
	    }
	} catch (IOException ioe) {
	    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ioe);
	}
	return false;
    }

    /**
     * Solicita informações para realizar a ação de mudar informações de uma nota
     * 
     * @return estado da tentativa de mudança da nota
     * @author Gleydvan e João Vitor
     * @since 15/09/2018
     * 
     */
    public boolean replaceNote() {
	String title;
	String content;
	int noteID;

	// System.out.print("Digite o titulo da Nota que deseja substituir: ");

	if (this.searchNoteByTitle()) {
	    System.out.println("Escolha qual a Nota que deseja substituir: ");
	    for (int i = 0; i < this.notes.size(); ++i) {
		System.out.println("Nota " + i + ": ");
		System.out.println(notes.get(i).toString());

	    }

	    System.out.print("Digite o numero da nota que gostaria de Substituir: ");
	    String id = MainClient.leitor.nextLine();
	    noteID = Integer.parseInt(id);

	    title = this.notes.get(noteID).getTitle();
	    System.out.println("Digite o novo conteudo: ");
	    content = MainClient.leitor.nextLine();

	    Note newNote = new Note(this.user, content, title, getDataTime());

	    ResponseClient answer = null;
	    try {
		Client.send(socket, client.put(PATHNOTES + "a_substituir-" + user + "-" + noteID, newNote.toString()));
		while (answer == null) {
		    answer = ResponseClient.read(socket.getInputStream());
		}
		if (answer.getCode().equals("201")) {
		    return true;
		} else {
		    // TODO(Someone): Tratar os possiveis erros
		}
	    } catch (IOException ioe) {
		Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ioe);
	    }
	    return false;
	}
	return false;
    }

    /**
     * Retorna o usuário
     * 
     * @return usuário
     * @author Gleydvan
     * @since 15/09/2018
     * 
     */
    public String getUser() {
	return user;
    }

    /**
     * Salva um usuário
     * 
     * @author Gleydvan
     * @since 15/09/2018
     * 
     */
    public void setUser(String user) {
	this.user = user;
    }

    /**
     * Retorna um array de notas
     * 
     * @return um array de notas
     * @author Gleydvan
     * @since 15/09/2018
     * 
     */
    public List<Note> getNotes() {
	return notes;
    }

    /**
     * Salva um array de notas
     * 
     * @author Gleydvan
     * @since 15/09/2018
     * 
     */
    public void setNotes(List<Note> notes) {
	this.notes = notes;
    }

    /**
     * Fecha a comunicação do socket
     * 
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void close() {
	try {
	    Client.send(socket, Client.LOGOFF);
	    socket.close();
	} catch (IOException ioe) {
	    Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ioe);
	}
    }

    /**
     * Recupera as notas armazenadas na string recebida como resposta do servidor
     * 
     * @param conteudoResposta
     *            Conteudo que deve conter notas a serem recuperadas
     * @return Lista com as notas recuperadas
     * @author
     * @throws IOException
     * @since 15/09/2018
     */
    private List<Note> retrieveNotes(String conteudoResposta) throws IOException {
	List<Note> list = new ArrayList<>();
	InputStream targetStream = new ByteArrayInputStream(conteudoResposta.getBytes());
	BufferedReader br = new BufferedReader(new InputStreamReader(targetStream));
	String title = "";
	String date = "";
	String user = "";

	while (br.ready()) {
	    String linha = br.readLine();
	    if (linha.contains("Title: ")) {
		String[] userLine = linha.split(" ");
		title = userLine[1].replace("\n", "");

		linha = br.readLine();
		userLine = linha.split(" ");
		date = userLine[1].replace("\n", "");

		linha = br.readLine(); // user
		userLine = linha.split(" ");
		user = userLine[1].replace("\n", "");

		linha = br.readLine(); // content:
		StringBuilder content = new StringBuilder();
		linha = br.readLine();
		while (linha != null && !linha.equals("#")) {
		    content.append(linha);
		    linha = br.readLine();
		}
		list.add(new Note(user, content.toString(), title, date));
	    }
	}
	return list;
    }
}
