package clientSide;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Classe responsável por manter os cabeçalhos do Cliente
 * 
 * @author João Vitor
 * @since 15/09/2018
 * @version
 * 
 */
public class Client {
	private static final String END = "\r\n";
	private static final String PROTOCOL = " HTTP/1.1" + END;
	private static final String USER_AGENT = "User-Agent: standard-client" + END;
	private static final String KEEP_ALIVE = "Connection: keep-alive" + END;
	private static final String CLOSE = "Connection: close" + END;

	private static final String CONTENT_TYPE = "Content-Type: ";
	private static final String CONTENT_LENGTH = "Content-Lenght: ";

	public static final String LOGOFF = "PUT logoff" + PROTOCOL + USER_AGENT + CLOSE + END;

	private final String server;

	private final String host;

	/**
	 * Construtor que salva as informações do servidor
	 * 
	 * @param server
	 * @since 15/09/2018
	 */
	public Client(final String server) {
		host = "Host: " + server + END;
		this.server = server;
	}

	/**
	 * Retorna o host da comunicação
	 * 
	 * @return host da comunicação
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public final String getHost() {
		return host;
	}

	/**
	 * Retorna o server da comunicação
	 * 
	 * @return server da comunicação
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public final String getServer() {
		return server;
	}

	/**
	 * Retorna uma composição de uma requisição CONNECT ao server
	 * 
	 * @return uma composição de uma requisição CONNECT ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String connect() {
		return "" + "CONNECT " + server + PROTOCOL + host + USER_AGENT + KEEP_ALIVE + END;
	}

	/**
	 * Retorna uma composição de uma requisição GET ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @return uma composição de uma requisição GET ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String get(final String resource) {
		return "" + "GET " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE + END;
	}

	/**
	 * Retorna uma composição de uma requisição HEAD ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @return uma composição de uma requisição HEAD ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String head(final String resource) {
		return "" + "HEAD " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE + END;
	}

	/**
	 * Retorna uma composição de uma requisição DELETE ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @return uma composição de uma requisição DELETE ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String delete(final String resource) {
		return "" + "DELETE " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE + END;
	}

	/**
	 * Retorna uma composição de uma requisição OPTIONS ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @return uma composição de uma requisição OPTIONS ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String options(final String resource) {
		return "" + "OPTIONS " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE + END;
	}

	/**
	 * Retorna uma composição de uma requisição TRACE ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @return uma composição de uma requisição TRACE ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String trace(final String resource) {
		return "" + "TRACE " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE + END;
	}

	/**
	 * Retorna uma composição de uma requisição POST ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @param entity
	 *            entidade da comunicação
	 * @return uma composição de uma requisição POST ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String post(final String resource, final String entity) {
		String request = "" + "POST " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE;
		request = request + CONTENT_TYPE + "text" + END;
		request = request + CONTENT_LENGTH + entity.length() + END;
		// TODO(Someone): tratar a entity...
		request = request + END + entity + END + END;
		return request;
	}

	/**
	 * Retorna uma composição de uma requisição PUT ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @param entity
	 *            entidade da comunicação
	 * @return uma composição de uma requisição PUT ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String put(final String resource, final String entity) {
		String request = "" + "PUT " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE;
		request = request + CONTENT_TYPE + "TODO" + END;
		request = request + CONTENT_LENGTH + entity.length() + END;
		// TODO(Someone): tratar a entity...
		request = request + END + entity + END + END;
		return request;
	}

	/**
	 * Retorna uma composição de uma requisição PATCH ao server
	 * 
	 * @param resource
	 *            recurso utilizado na comunicação
	 * @param entity
	 *            entidade da comunicação
	 * @return uma composição de uma requisição PATCH ao server
	 * @author João Vitor
	 * @since 15/09/2018
	 * 
	 */
	public String patch(final String resource, final String entity) {
		String request = "" + "PATCH " + resource + PROTOCOL + host + USER_AGENT + KEEP_ALIVE;
		request = request + CONTENT_TYPE + "TODO" + END;
		request = request + CONTENT_LENGTH + entity.length() + END;
		// TODO(Someone): tratar a entity...
		request = request + END + entity + END + END;
		return request;
	}

	/**
	 * Retorna o IP da maquina
	 * 
	 * @return Ip ou null
	 * @author João Vitor
	 * @since 15/09/2018
	 * @throws SocketException
	 * 
	 */
	public static String getIP() throws SocketException {
		final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
		while (interfaces.hasMoreElements()) {
			final NetworkInterface iface = interfaces.nextElement();
			if (iface.isLoopback() || !iface.isUp() || iface.isVirtual() || iface.isPointToPoint()) {
				continue;
			}

			final Enumeration<InetAddress> addresses = iface.getInetAddresses();
			while (addresses.hasMoreElements()) {
				final InetAddress addr = addresses.nextElement();

				final String ip = addr.getHostAddress();
				if (Inet4Address.class == addr.getClass()) {
					return ip;
				}
			}
		}
		return null;
	}

	/**
	 * Manda a requisição para o server associado ao socket informado.
	 * 
	 * @param socket
	 *            Socket a permirtir a comunicação
	 * @param request
	 *            Requisição a ser enviada
	 * @throws IOException
	 *             Exceção caso o envio dê errado
	 * @author João Vitor
	 * @since 15/09/2018
	 */
	public static void send(Socket socket, String request) throws IOException {
		final OutputStream ostream = socket.getOutputStream();
		final byte[] bytes = request.getBytes();
		ostream.write(bytes);
		//System.out.println("Cliente Mandou pro server\n" + request);
		ostream.flush();
	}
}