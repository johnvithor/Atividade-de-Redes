package commonSide;

/**
 * Classe que implementa a ideia de um nota
 * 
 * @author Gleydvan
 * @since 15/09/2018
 * @version
 * 
 */
public class Note {
	private String date;
	private String user;
	private String content;
	private String title;

	/**
	 * Construtor que possibilita o armazenamento de um nota
	 * 
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public Note(String user, String content, String title, String date) {
		this.date = date;
		this.user = user;
		this.content = content;
		this.title = title;
	}

	/**
	 * Retorna a data da nota
	 * 
	 * @return Data da nota
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Salva a data da nota
	 * 
	 * @param date Data da nota
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Retorna o usuário da nota
	 * 
	 * @return Nome do usuário
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Salva o usuário da nota
	 * 
	 * @param user Usuário da nota
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Retorna o conteúdo da nota
	 * 
	 * @return Conteúdo da nota
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Salva o conteúdo da nota
	 * 
	 * @param content Conteúdo da nota
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Retorna o título da nota
	 * 
	 * @return Título da nota
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Salva o título da nota
	 * 
	 * @param title Título da nota
	 * @author Gleydvan
	 * @since 15/09/2018
	 * 
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Title: " + title + "\n" + "Date: " + date + "\n" + "User: " + user + "\n" + "Content:\n" + content
				+ "\n";
	}
}
