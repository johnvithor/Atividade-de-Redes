package serverSide;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreadConnection implements Runnable {

    private final Socket socket;
    private boolean connected;

    public ThreadConnection(Socket socket) {
	this.socket = socket;
    }

    // converte o formato para o GMT espeficicado pelo protocolo HTTP
    public String getFormattedData() {
	SimpleDateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssX");
	gmtDateFormat.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
	return gmtDateFormat.format(new Date());
    }

    public static String getIP() throws SocketException {
	Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
	while (interfaces.hasMoreElements()) {
	    NetworkInterface iface = interfaces.nextElement();
	    if (iface.isLoopback() || !iface.isUp() || iface.isVirtual() || iface.isPointToPoint()) {
		continue;
	    }

	    Enumeration<InetAddress> addresses = iface.getInetAddresses();
	    while (addresses.hasMoreElements()) {
		InetAddress addr = addresses.nextElement();

		final String ip = addr.getHostAddress();
		if (Inet4Address.class == addr.getClass()) {
		    return ip;
		}
	    }
	}
	return null;
    }

    private AnswerServer buildAnswer(RequestServer request) throws IOException {
	AnswerServer answer;
	switch (request.getCommand()) {
	case GET:
	    answer = Command.get(request);
	    break;
	case HEAD:
	    answer = Command.head(request);
	    break;
	case DELETE:
	    answer = Command.delete(request);
	    break;
	case CONNECT:
	    answer = Command.connect(request);
	    break;
	case PUT:
	    answer = Command.put(request);
	    break;
	case POST:
	    answer = Command.post(request);
	    break;
	case PATCH:
	    answer = Command.patch(request);
	    break;
	case OPTIONS:
	    answer = Command.options(request);
	    break;
	case TRACE:
	    answer = Command.trace(request);
	    break;
	default:
	    // TODO(Someone): Tratar esse erro.
	    connected = false;
	    return new AnswerServer("HTTP", 400, "Bad Request");
	}
	// cabeçalho padrão da resposta HTTP/1.1
	answer.newHeader("Location", getIP());
	answer.newHeader("Date", getFormattedData());
	answer.newHeader("Server", "Standard-Server/1.0");
	if (answer.getContent() != null) {
	    answer.newHeader("Content-Type", "text/html");
	    answer.newHeader("Content-Length", answer.getLengthAnswer());
	}

	answer.setExitChannel(socket.getOutputStream());
	return answer;
    }

    @Override
    public void run() {
	connected = true;
	// imprime na tela o IP do cliente
	// System.out.println("Enrereço do cliente: " + socket.getInetAddress());
	while (connected) {
	    try {
		long startTime = System.currentTimeMillis();
		RequestServer request = RequestServer.read(socket.getInputStream());
		if (request != null) {
		    if (request.isKeepAlive()) {
			socket.setKeepAlive(true);
			socket.setSoTimeout(0);
		    } else {
			socket.setSoTimeout(300);
		    }
		    if (request.getResource().trim().equals("logoff")) {
			System.out.println("Closing socket logoff");
			connected = false;
		    } else {
			AnswerServer answer = buildAnswer(request);
			answer.send();
			long endTime = System.currentTimeMillis();
			System.out.println("Levou " + (endTime - startTime) + " milesegundos");
		    }
		}
	    } catch (SocketTimeoutException ste) {
		try {
		    connected = false;
		    System.out.println("Closing socket timeout");
		    socket.close();
		} catch (IOException ioe) {
		    Logger.getLogger(ThreadConnection.class.getName()).log(Level.SEVERE, null, ioe);
		}
	    } catch (IOException ioe) {
		Logger.getLogger(ThreadConnection.class.getName()).log(Level.SEVERE, null, ioe);
	    } catch (NullPointerException npr) {
		try {
		    connected = false;
		    System.out.println("Closing socket null");
		    socket.close();
		} catch (IOException ioe) {
		    Logger.getLogger(ThreadConnection.class.getName()).log(Level.SEVERE, null, ioe);
		}
		Logger.getLogger(ThreadConnection.class.getName()).log(Level.SEVERE, null, npr);
	    }
	}
	try {
	    socket.close();
	} catch (IOException ioe) {
	    Logger.getLogger(ThreadConnection.class.getName()).log(Level.SEVERE, null, ioe);
	}
	System.out.println("End Connection\n");
    }
}