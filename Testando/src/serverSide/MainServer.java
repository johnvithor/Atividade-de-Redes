package serverSide;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Classe responsável por criar uma conexões com o servidor através do Socket
 * 
 * @author João Vitor
 * @since 15/09/2018
 * 
 */
public class MainServer {
    /**
     * Cria conexões com o servidor através do Socket e as associam a Threads
     * 
     * @param args
     * @author João Vitor
     * @since 15/09/2018
     * @throws IOException
     * 
     */
    public static void main(String[] args) throws IOException {
	ServerSocket server = new ServerSocket(8000);
	// executor que limita a criação de threads a 20
	ExecutorService pool = Executors.newFixedThreadPool(20);
	System.out.println("Server Ligado\n");
	while (!server.isClosed()) {
	    // cria uma nova thread para cada nova solicitacao de conexao
	    pool.execute(new ThreadConnection(server.accept()));
	}
	server.close();
    }
}
