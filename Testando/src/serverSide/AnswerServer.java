package serverSide;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Classe responsável pelo armazenamento das respostas do Servidor
 * 
 * @author João Vitor
 * @since 15/09/2018
 * @version
 * 
 */
public class AnswerServer {

    private String protocol;
    private int answerCode;
    private String mensage;
    private byte[] content;
    private Map<String, List> headers;
    private OutputStream exitChannel;

    public AnswerServer() {

    }

    public AnswerServer(String protocolo, int codigoResposta, String mensagem) {
	this.protocol = protocolo;
	this.answerCode = codigoResposta;
	this.mensage = mensagem;
    }

    /**
     * Envia os dados da resposta ao cliente.
     *
     * @throws IOException
     */
    public void send() throws IOException {
	// System.out.println("Server manda pra cliente:\n" + this.toString());
	exitChannel.write(mountHeaders());
	if (content != null) {
	    exitChannel.write(content);
	}
	exitChannel.flush();
    }

    /**
     * Salva os cabeçalhos e os associam a uma lista de valores no Mapa
     *
     * @param key
     *            Nome do cabeçalho
     * @param values
     *            lista com um ou mais valores para esta chave
     * @since 15/09/2018
     */
    public void newHeader(String key, String... values) {
	if (headers == null) {
	    headers = new TreeMap<>();
	}
	headers.put(key, Arrays.asList(values));
    }

    /**
     * Retorna o tamanho do conteúdo da resposta
     *
     * @return bytes do conteúdo da resposta convertido para String
     * @author João Vitor
     * @since 15/09/2018
     */
    public String getLengthAnswer() {
	return getContent().length + "";
    }

    /**
     * Retorna os bytes do conteúdo da resposta
     * 
     * @return o valor em bytes do conteúdo da resposta
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public byte[] getContent() {
	return content;
    }

    /**
     * Converte o cabecalho em string.
     * 
     * @return retorna o cabecalho em bytes
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    private byte[] mountHeaders() {
	return this.toString().getBytes();
    }

    @Override
    public String toString() {
	StringBuilder str = new StringBuilder();
	str.append(protocol).append(" ").append(answerCode).append(" ").append(mensage).append("\r\n");
	for (Map.Entry<String, List> entry : headers.entrySet()) {
	    str.append(entry.getKey());
	    String stringCorrigida = Arrays.toString(entry.getValue().toArray()).replace("[", "").replace("]", "");
	    str.append(": ").append(stringCorrigida).append("\r\n");
	}
	str.append("\r\n");
	return str.toString();
    }

    /**
     * Atribui valor a saida
     * 
     * @param outputStream
     *            Stream de saída
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void setExitChannel(OutputStream outputStream) {
	exitChannel = outputStream;

    }

    /**
     * Salva os bytes do conteudo da resposta
     * 
     * @param newContent
     *            bytes da resposta
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public void setContent(byte[] newContent) {
	content = newContent;

    }
}