package serverSide;

import java.io.IOException;
import java.util.List;

import commonSide.Note;

/**
 * Classe responsável por centralizar a criação das respostas para cada
 * requisição obtida
 * 
 * @author João Vitor
 * @since 15/09/2018
 * 
 */
public class Command {
    private static final String OK = "OK";
    private static final String BADREQUEST = "Bad Request";
    private static final String NOTFOUND = "Not Found";
    private static final String CONFLICT = "Conflict";
    private static final String SERVICEUNAVAILABLE = "Service Unavailable";
    private static final String UNAUTHORIZED = "Unauthorized";
    private static final String CREATED = "Created";
    private static final String FOUND = "Found";

    // private static final String NOCONTENT = "No Content"; // 204

    private static final String PATHNOTES = "caminho_para_notas";

    /**
     * Retorna a respota da requisção GET feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção GET feita
     * @author João Vitor
     * @throws IOException
     * @since 15/09/2018
     * 
     */
    public static AnswerServer get(RequestServer request) throws IOException {
	AnswerServer answer;
	String[] recurso = request.getResource().trim().split("-");
	if (recurso.length != 3 || !recurso[0].equals(PATHNOTES)) {
	    answer = new AnswerServer(request.getProtocol(), 400, BADREQUEST);
	} else {
	    List<Note> result = DataManager.search(recurso[1], recurso[2]);
	    if (result.isEmpty()) {
		answer = new AnswerServer(request.getProtocol(), 404, NOTFOUND);
	    } else if (result.size() == 1) {
		answer = new AnswerServer(request.getProtocol(), 302, FOUND);
		String resultstring = result.toString();
		answer.setContent(resultstring.getBytes());
	    } else {
		answer = new AnswerServer(request.getProtocol(), 409, CONFLICT);
		answer.setContent(result.toString().getBytes());
	    }
	}

	return answer;
    }

    /**
     * Retorna a respota da requisção HEAD feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção HEAD feita
     * @author João Vitor
     * @throws IOException
     * @since 15/09/2018
     * 
     */
    public static AnswerServer head(RequestServer request) throws IOException {
	AnswerServer answer;
	String[] recurso = request.getResource().trim().split("-");
	if (recurso.length != 3 || !recurso[0].equals(PATHNOTES)) {
	    answer = new AnswerServer(request.getProtocol(), 400, BADREQUEST);
	} else {
	    List<Note> result = DataManager.search(recurso[1], recurso[2]);
	    if (result.isEmpty()) {
		answer = new AnswerServer(request.getProtocol(), 404, NOTFOUND);
	    } else if (result.size() == 1) {
		answer = new AnswerServer(request.getProtocol(), 302, FOUND);
	    } else {
		answer = new AnswerServer(request.getProtocol(), 409, CONFLICT);
	    }
	}

	return answer;
    }

    /**
     * Retorna a respota da requisção CONNECT feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção CONNECT feita
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public static AnswerServer connect(RequestServer request) {
	AnswerServer answer;
	answer = new AnswerServer(request.getProtocol(), 200, OK);
	return answer;
    }

    /**
     * Retorna a respota da requisção DELETE feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção DELETE feita
     * @author João Vitor
     * @throws IOException
     * @since 15/09/2018
     * 
     */
    public static AnswerServer delete(RequestServer request) throws IOException {
	AnswerServer answer;
	String[] recurso = request.getResource().trim().split("-");
	if (recurso[0].equals(PATHNOTES) && recurso.length == 3) {
	    List<Note> result = DataManager.delete(recurso[1], recurso[2]);
	    if (result.isEmpty()) {
		answer = new AnswerServer(request.getProtocol(), 404, NOTFOUND);
	    } else if (result.size() == 1) {
		if (result.get(0).getTitle().equals("ok")) {
		    answer = new AnswerServer(request.getProtocol(), 200, OK);
		} else {
		    answer = new AnswerServer(request.getProtocol(), 503, SERVICEUNAVAILABLE);
		}
	    } else {
		answer = new AnswerServer(request.getProtocol(), 409, CONFLICT);
		answer.setContent(result.toString().getBytes());
	    }
	} else if (recurso[0].equals(PATHNOTES + "da_lista_enviada") && recurso.length == 4) {
	    if (DataManager.delete(recurso[1], recurso[2], recurso[3])) {
		answer = new AnswerServer(request.getProtocol(), 200, OK);
	    } else {
		answer = new AnswerServer(request.getProtocol(), 503, SERVICEUNAVAILABLE);
	    }
	} else {
	    answer = new AnswerServer(request.getProtocol(), 400, BADREQUEST);
	}

	return answer;
    }

    /**
     * Retorna a respota da requisção POST feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção POST feita
     * @author João Vitor
     * @throws IOException
     * @since 15/09/2018
     * 
     */
    public static AnswerServer post(RequestServer request) throws IOException {
	AnswerServer answer;
	if (request.getResource().equals("caminho_para_login")) {
	    String[] data = request.getContent().split("-");
	    if (DataManager.hasUser(data[0]) && DataManager.login(data[0], data[1])) {
		answer = new AnswerServer(request.getProtocol(), 200, OK);
	    } else {
		answer = new AnswerServer(request.getProtocol(), 401, UNAUTHORIZED);
	    }
	} else {
	    String[] recurso = request.getResource().trim().split("-");
	    if (recurso.length != 2 || !recurso[0].equals(PATHNOTES)) {
		answer = new AnswerServer(request.getProtocol(), 400, BADREQUEST);
	    } else {
		DataManager bd = new DataManager(recurso[1]);
		if (bd.newNote(recurso[1], request.getContent())) {
		    answer = new AnswerServer(request.getProtocol(), 201, CREATED);
		} else {
		    answer = new AnswerServer(request.getProtocol(), 503, SERVICEUNAVAILABLE);
		}
	    }
	}
	return answer;
    }

    /**
     * Retorna a respota da requisção PUT feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção PUT feita
     * @author João Vitor
     * @throws IOException
     * @since 15/09/2018
     * 
     */
    public static AnswerServer put(RequestServer request) throws IOException {
	AnswerServer answer;
	String[] recurso = request.getResource().trim().split("-");
	if (recurso.length == 1 && recurso[0].equals("logoff")) {
	    answer = new AnswerServer(request.getProtocol(), 200, OK);
	} else if (recurso.length != 3 || !recurso[0].equals(PATHNOTES + "a_substituir")) {
	    answer = new AnswerServer(request.getProtocol(), 400, BADREQUEST);
	} else {
	    if (DataManager.replace(recurso[1], recurso[2], request.getContent())) {
		answer = new AnswerServer(request.getProtocol(), 201, CREATED);
	    } else {
		answer = new AnswerServer(request.getProtocol(), 503, SERVICEUNAVAILABLE);
	    }
	}
	return answer;
    }

    /**
     * Retorna a respota da requisção PATCH feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção PATCH feita
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public static AnswerServer patch(RequestServer request) {
	AnswerServer answer;
	answer = new AnswerServer(request.getProtocol(), 200, OK);
	return answer;
    }

    /**
     * Retorna a respota da requisção OPTIONS feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção OPTIONS feita
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public static AnswerServer options(RequestServer request) {
	AnswerServer answer;
	answer = new AnswerServer(request.getProtocol(), 200, OK);
	return answer;
    }

    /**
     * Retorna a respota da requisção TRACE feita
     * 
     * @param request
     *            requisição feita
     * @return a respota da requisção TRACE feita
     * @author João Vitor
     * @since 15/09/2018
     * 
     */
    public static AnswerServer trace(RequestServer request) {
	AnswerServer answer;
	answer = new AnswerServer(request.getProtocol(), 200, OK);
	return answer;
    }
}
