package serverSide;

public enum CommandType {
    GET, HEAD, CONNECT, POST, PUT, OPTIONS, DELETE, TRACE, PATCH, INVALID;
}
