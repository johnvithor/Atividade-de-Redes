package serverSide;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import commonSide.Note;

/**
 * Classe responsável por simular um Banco de Dados
 * 
 * @author Ailson F. dos Santos and Rodolpho Erick and João Vitor
 * @since 15/09/2018
 * @version
 * 
 */
public class DataManager {

    private static File authFile = new File("auth/src.txt");;
    private static List<Note> temp = new ArrayList<>();

    private File file;

    /**
     * Default constructor
     * 
     * @author Ailson F. dos Santos
     * @throws IOException
     */
    public DataManager() throws IOException {
	file = new File("docs/generalNotes.txt");
	int count = 0;
	while (!isFileCreated(authFile) || count++ < 50) {
	    Logger.getLogger(DataManager.class.getName()).log(Level.WARNING, null,
		    "Error creating file: " + authFile.getPath() + " [Try " + Integer.toString(count) + "/50]");
	}
	while (!isFileCreated(file) || count++ < 50) {
	    Logger.getLogger(DataManager.class.getName()).log(Level.WARNING, null,
		    "Error creating file: " + file.getPath() + " [Try " + Integer.toString(count) + "/50]");
	}
    }

    /**
     * Constructor
     * 
     * @author Ailson F. dos Santos
     * @param userName
     * @throws IOException
     */
    public DataManager(String userName) throws IOException {
	file = new File("docs/" + userName + ".txt");
	int count = 0;
	while (!isFileCreated(authFile) || count++ < 50) {
	    Logger.getLogger(DataManager.class.getName()).log(Level.WARNING, null,
		    "Error creating file: " + authFile.getPath() + " [Try " + Integer.toString(count) + "/50]");
	}
	while (!isFileCreated(file) || count++ < 50) {
	    Logger.getLogger(DataManager.class.getName()).log(Level.WARNING, null,
		    "Error creating file: " + file.getPath() + " [Try " + Integer.toString(count) + "/50]");
	}
    }

    /**
     * Method to validate the authentication file for this pseudo DB.
     * 
     * @author Ailson F. dos Santos and Rpdolpho Erick
     * @return true if file is created and false otherwise.
     * @throws IOException
     */
    public boolean isFileCreated(File file) throws IOException {
	if (!file.exists())
	    return file.createNewFile();
	else
	    return true;
    }

    /**
     * Check whether user exists or not on DataBase
     * 
     * @author Ailson F. dos Santos
     * @param user
     * @return true if user does exist
     * @throws IOException
     */
    public static boolean hasUser(String user) throws IOException {
	FileReader fr = new FileReader(authFile);
	boolean retorno = false;
	int countUserName = 0;
	while (new BufferedReader(fr).ready()) {
	    String linha = new BufferedReader(fr).readLine();

	    if (linha.contains(user + " ")) {
		Logger.getLogger(DataManager.class.getName()).log(Level.INFO, null, "User " + user + " exists");
		retorno = true;
		countUserName++;
		if (countUserName > 1) {
		    Logger.getLogger(DataManager.class.getName()).log(Level.SEVERE, null,
			    "User " + user + " is duplicated");
		    duplicatedUserNameExceptionThrow(user);
		}
	    }
	}
	if (!retorno) {
	    Logger.getLogger(DataManager.class.getName()).log(Level.WARNING, null, "User " + user + " does not exist");
	}
	new BufferedReader(fr).close();
	fr.close();
	return retorno;
    }

    /**
     * Check if password is correct
     * 
     * @author Ailson F. dos Santos
     * @param user
     * @param password
     * @return true if password match
     * @throws IOException
     */
    public static boolean login(String user, String password) throws IOException {
	FileReader fr = new FileReader(authFile);
	boolean retorno = false;
	int countUserName = 0;
	while (new BufferedReader(fr).ready()) {
	    String linha = new BufferedReader(fr).readLine();

	    if (linha.contains(user + " ")) {
		String[] userLine = linha.split(" ");
		if (password.replace("\n", "").replace("\r", "").equals(userLine[1])) {
		    retorno = true;
		}
		countUserName++;
		if (countUserName > 1) {
		    Logger.getLogger(DataManager.class.getName()).log(Level.SEVERE, null,
			    "User " + user + " is duplicated. User not logged");
		    retorno = false;
		    duplicatedUserNameExceptionThrow(user);
		}
	    }
	}
	if (!retorno) {
	    Logger.getLogger(DataManager.class.getName()).log(Level.WARNING, null, "User " + user + " does not exist");
	} else {
	    Logger.getLogger(DataManager.class.getName()).log(Level.INFO, null, "User " + user + " logged");
	}
	new BufferedReader(fr).close();
	fr.close();
	return retorno;
    }

    /**
     * Send throw exception when duplicated user is detected
     * 
     * @param user
     * @throws IOException
     */
    private static void duplicatedUserNameExceptionThrow(String user) throws IOException {
	throw new IOException("User " + user + " is duplicated.");
    }

    /**
     * Search for notes with key
     * 
     * @param key
     * @return
     */
    public List<Note> search(String key) {
	return new ArrayList<>();
    }

    /**
     * Search for notes with key
     * 
     * @param user
     * @param key
     * @return
     * @throws IOException
     */
    public static List<Note> search(String user, String key) throws IOException {
	List<Note> result = new ArrayList<>();

	String title = "";
	String date = "";

	FileReader fr = new FileReader("docs/" + user + ".txt");
	BufferedReader br = new BufferedReader(fr);

	String line = "";
	String previousLine = "";
	while (br.ready()) {
	    previousLine = line;
	    line = br.readLine();

	    if (line.equals("Title: " + key.replace("\n", ""))) {
		String[] userLine = line.split(" ");
		title = userLine[1].replace("\n", "");

		line = br.readLine();
		userLine = line.split(" ");
		date = userLine[1].replace("\n", "");

		line = br.readLine(); // user
		line = br.readLine(); // content:
		StringBuilder content = new StringBuilder();
		line = br.readLine();
		while (!line.equals("") && br.ready() && !line.contains("Title:")) {
		    content.append(line);
		    line = br.readLine();
		}
		result.add(new Note(user, content.toString() + "\n#", title, date));
	    } else if (line.contains("Date: " + key.replace("/", "-"))) {
		String[] userLine = previousLine.split(" ");
		title = userLine[1].replace("\n", "");

		userLine = line.split(" ");
		date = userLine[1].replace("\n", "");

		line = br.readLine(); // user
		line = br.readLine(); // content:
		StringBuilder content = new StringBuilder();
		line = br.readLine();
		while (!line.equals("") && br.ready() && !line.contains("Title:")) {
		    content.append(line);
		    line = br.readLine();
		}
		result.add(new Note(user, content.toString() + "\n#", title, date));
	    }
	}
	result.get(result.size() - 1).setContent(result.get(result.size() - 1).getContent() + "\nend");
	br.close();
	fr.close();
	temp = result;
	return result;
    }

    /**
     * Delete note
     * 
     * @param user
     * @param title
     * @return
     * @throws IOException
     */
    public static List<Note> delete(String user, String title) throws IOException {
	List<Note> result = new ArrayList<>();
	String date = "";
	ArrayList<String> linesToDelete = new ArrayList<>();
	FileReader fr = new FileReader("docs/" + user + ".txt");
	BufferedReader br = new BufferedReader(fr);

	String line = "";
	while (br.ready()) {
	    line = br.readLine();
	    if (line.equals("Title: " + title.replace("\n", ""))) {
		linesToDelete.add(line);
		String[] userLine = line.split(" ");
		title = userLine[1].replace("\n", "");

		line = br.readLine();
		linesToDelete.add(line);
		userLine = line.split(" ");
		date = userLine[1].replace("\n", "");

		line = br.readLine(); // user
		linesToDelete.add(line);
		line = br.readLine(); // content:
		linesToDelete.add(line);
		StringBuilder content = new StringBuilder();
		line = br.readLine();
		linesToDelete.add(line);
		while (!line.equals("") && br.ready() && !line.contains("Title:")) {
		    content.append(line);
		    line = br.readLine();
		    linesToDelete.add(line);
		}
		result.add(new Note(user, content.toString() + "\n#", title, date));
	    }
	}
	if (result.size() == 1) {
	    File inputFile = new File("docs/" + user + ".txt");
	    File tempFile = new File("docs/" + user + ".temp");

	    BufferedReader reader = new BufferedReader(new FileReader(inputFile));
	    BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

	    String currentLine;
	    int count = 0;
	    while ((currentLine = reader.readLine()) != null) {
		// trim newline when comparing with lineToRemove
		String trimmedLine = currentLine.trim();
		boolean found = false;
		for (String delete : linesToDelete) {
		    if (trimmedLine.equals(delete) && count < linesToDelete.size()) {
			found = true;
			++count;
			break;
		    }
		}
		if (!found) {
		    writer.write(currentLine + System.getProperty("line.separator"));
		}

	    }
	    writer.close();
	    reader.close();
	    if (tempFile.renameTo(inputFile)) {
		result.get(0).setTitle("ok");
	    }
	}
	br.close();
	fr.close();
	result.get(result.size() - 1).setContent(result.get(result.size() - 1).getContent() + "\nend");
	temp = result;
	return result;
    }

    /**
     * Delete note
     * 
     * @param user
     * @param title
     * @param at
     * @return
     * @throws IOException
     */
    public static boolean delete(String user, String title, String at) throws IOException {
	int ati = Integer.parseInt(at);
	int count = -1;
	String[] linesToDelete = temp.get(ati).toString().split("\n");

	File inputFile = new File("docs/" + user + ".txt");
	File tempFile = new File("docs/" + user + ".temp");

	BufferedReader reader = new BufferedReader(new FileReader(inputFile));
	BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

	String currentLine;

	while ((currentLine = reader.readLine()) != null) {
	    // trim newline when comparing with lineToRemove
	    String trimmedLine = currentLine.trim();
	    if (trimmedLine.equals("Title: " + title.replace("\n", ""))) {
		++count;
	    }
	    boolean found = false;
	    for (String delete : linesToDelete) {
		if (trimmedLine.contains(delete) && count == ati) {
		    found = true;
		    break;
		}
	    }
	    if (!found) {
		writer.write(currentLine + System.getProperty("line.separator"));
	    }
	}
	writer.close();
	reader.close();

	return tempFile.renameTo(inputFile);
    }

    /**
     * Create new note on users diary
     * 
     * @author Rodolpho Erick
     * @param user
     * @param note
     * @return
     * @throws IOException
     */
    public boolean newNote(String user, String note) throws IOException {
	file = new File("docs/" + user + ".txt");

	if (isFileCreated(file)) {
	    FileWriter fw = new FileWriter(file, true);
	    BufferedWriter bw = new BufferedWriter(fw);

	    bw.write(note);
	    bw.newLine();

	    bw.close();
	    fw.close();

	    return true;
	}

	return false;
    }

    /**
     * Replace note
     * 
     * @param user
     * @param number
     * @param note
     * @return
     * @throws IOException
     */
    public static boolean replace(String user, String number, String note) throws IOException {
	int ati = Integer.parseInt(number);
	int count = -1;
	String[] linesToReplace = note.split("\n");
	String[] linesOldToReplace = temp.get(ati).toString().split("\n");

	File inputFile = new File("docs/" + user + ".txt");
	File tempFile = new File("docs/" + user + ".temp");

	BufferedReader reader = new BufferedReader(new FileReader(inputFile));
	BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

	String currentLine;

	while ((currentLine = reader.readLine()) != null) {
	    // trim newline when comparing with lineToRemove
	    String trimmedLine = currentLine.trim();
	    if (trimmedLine.equals(linesToReplace[0].replace("\n", ""))) {
		++count;
	    }
	    for (int i = 0; i < linesOldToReplace.length; i++) {
		if (trimmedLine.contains(linesOldToReplace[i]) && count == ati) {
		    currentLine = linesToReplace[i];
		}
	    }
	    writer.write(currentLine + System.getProperty("line.separator"));
	}
	writer.close();
	reader.close();

	return tempFile.renameTo(inputFile);
    }
}
