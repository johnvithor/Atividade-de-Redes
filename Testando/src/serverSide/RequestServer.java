package serverSide;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class RequestServer {

    private String protocol = "null";
    private String resource = "null";
    private CommandType command;
    private String content;
    private boolean keepAlive = true;
    private int timeOut = 3000;
    private Map<String, List> headers = null;

    public static RequestServer read(InputStream inputStream) throws IOException {
	RequestServer request = new RequestServer();
	BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));

	String requestLine = buffer.readLine();
	if (requestLine != null) {
	    //System.out.println("Server recebe do Cliente:\n" + requestLine);

	    String[] header = requestLine.split(" ");
	    if (header.length != 3) {
		return request;
	    }

	    // pega o metodo
	    request.setCommand(header[0]);
	    // paga o caminho do arquivo
	    request.setResource(header[1]);
	    // pega o protocolo
	    request.setProtocol(header[2]);

	    String dataHeaders = buffer.readLine();
	    while (dataHeaders != null && !dataHeaders.isEmpty()) {
		//System.out.println(dataHeaders);
		String[] linhaCabecalho = dataHeaders.split(":");
		request.newHeader(linhaCabecalho[0], linhaCabecalho[1].trim().split(","));
		dataHeaders = buffer.readLine();
	    }
	    if (request.getHeaders().containsKey("Connection")) {
		request.setKeepAlive(request.getHeaders().get("Connection").get(0).equals("keep-alive"));
	    }

	    StringBuilder data = new StringBuilder();
	    if (request.getCommand() == CommandType.POST || request.getCommand() == CommandType.PUT) {
		String dataBody = buffer.readLine();
		while (dataBody != null && !dataBody.isEmpty()) {
		    data.append(dataBody + '\n');
		    dataBody = buffer.readLine();
		}
		request.setContent(data.toString());
	    }

	    //System.out.println();
	    //System.out.println(data.toString());
	    return request;
	}
	return null;
    }

    public void newHeader(String chave, String... valores) {
	if (headers == null) {
	    headers = new TreeMap<>();
	}
	headers.put(chave, Arrays.asList(valores));
    }

    public Map<String, List> getHeaders() {
	return headers;
    }

    public String getProtocol() {
	return protocol;
    }

    public void setProtocol(String protocolo) {
	this.protocol = protocolo;
    }

    public String getResource() {
	return resource;
    }

    public void setResource(String recurso) {
	this.resource = recurso;
    }

    public CommandType getCommand() {
	return command;
    }

    public void setCommand(String command) {
	switch (command) {
	case "GET":
	    this.command = CommandType.GET;
	    break;
	case "HEAD":
	    this.command = CommandType.HEAD;
	    break;
	case "DELETE":
	    this.command = CommandType.DELETE;
	    break;
	case "CONNECT":
	    this.command = CommandType.CONNECT;
	    break;
	case "PUT":
	    this.command = CommandType.PUT;
	    break;
	case "POST":
	    this.command = CommandType.POST;
	    break;
	case "PATCH":
	    this.command = CommandType.PATCH;
	    break;
	case "OPTIONS":
	    this.command = CommandType.OPTIONS;
	    break;
	case "TRACE":
	    this.command = CommandType.TRACE;
	    break;
	default:
	    this.command = CommandType.INVALID;
	    break;
	}
    }

    public boolean isKeepAlive() {
	return keepAlive;
    }

    public void setKeepAlive(boolean keepAlive) {
	this.keepAlive = keepAlive;
    }

    public int getTimeOut() {
	return timeOut;
    }

    public void setTimeOut(int timeOut) {
	this.timeOut = timeOut;
    }

    public String getContent() {
	return content;
    }

    public void setContent(String newContent) {
	this.content = newContent;
    }
}